public class App {
    public static void main(String[] args) {
        RBTree rb = new RBTree();
        RBTree clone = rb.clone();

        System.out.println(rb.isEmpty());
        System.out.println(rb.isBalanced());

        rb.add(10, "pizza");
        rb.add(9, "sushi");
        rb.add(12, "severo");
        rb.add(14, "madero");
        
        System.out.println(rb.getComida(11));
        System.out.println(rb.positionsPre());
        System.out.println("=-=-=-=-=-=");
        System.out.println(rb.positionsCentral());
        System.out.println("=-=-=-=-=-=");
        System.out.println(rb.positionsPos());
//        System.out.println(rb.size());
//        System.out.println(rb.height());
//        System.out.println(rb.getParent(9));
//        System.out.println(rb.isBalanced());
//        System.out.println(rb.getRootElement());
//        System.out.println("=-=-=-=-=-=-=-=-=-=-=-=");
//        System.out.println(clone.size());
//        System.out.println(clone.height());
//        System.out.println(clone.getParent(9));
//        System.out.println(clone.isBalanced());
    }
}
