public class BinaryTree {

    private class Node {

        private Integer element;
        private Node father;
        private Node left;
        private Node right;

        public Node(Integer element) {
            this.element = element;
            this.left = null;
            this.right = null;
            this.father = null;
        }

    }


    private int count;
    private Node root;


    public BinaryTree() {
        this.count = 0;
        this.root = null;
    }

    public boolean setRoot(Integer element) {
        if (!isEmpty())
            return false;
        root = new Node(element);
        return true;
    }


    public boolean isEmpty() {
        return root == null;
    }

    /**
     * Limpa todos os elementos da arvore e reseta o count.
     */
    public void clear() {
        root = null;
        count = 0;
    }

    public void add(Integer element) {
        add(root, element);
        count++;
    }

    /**
     * Método que adiciona o elemento passado por parametro na arvore.
     * Recebe o nodo aonde vai ser feito a inserção e o elemento que deseja ser adicionado.
     * Caso a arvore esteja vazia, o elemento é adicionado como a raiz.
     * Também faz a verificação para qual lado deve ser inserido na arvore.
     */
    private Node add(Node n, Integer element) {
        Node aux = null;
        if (n == null) {
            setRoot(element);
            aux = new Node(element);
            return aux;
        }
        if (n != null) {
            if (element > n.element) {
                if (n.right == null) {
                    n.right = new Node(element);
                    aux = searchNodeRef(element, root);
                    aux.father = n;
                    aux.right = null;
                    aux.left = null;
                    return aux;
                } else if (n.right != null) {
                    add(n.right, element);
                }
            }
            if (element < n.element) {
                if (n.left == null) {
                    n.left = new Node(element);
                    aux = searchNodeRef(element, root);
                    aux.father = n;
                    aux.right = null;
                    aux.left = null;
                    return aux;
                } else if (n.left != null) {
                    add(n.left, element);
                }
            }
        }
        return null;
    }


//    /**
//     * Verifica os dois posssiveis cenarios que necessitam fazer uma rotação de nodos para a esquerda.
//     * Recebe como parametro o nodo que foi recentemente adicionado.
//     */
//    public void rotateNodeLeft(Node node) {
//        Node aux = node.father.right;
//        Node parent = node.father;
//        Node grandparent = parent.father;
//
//        if(grandparent.left.cor == Cor.RED && grandparent.left == parent && grandparent.right.cor == Cor.BLACK) {
//            aux.left = parent;
//            aux.father = grandparent;
//            grandparent.left = aux;
//            parent.father = aux;
//        }
//
//          if(parentOf(node) == leftOf(grandParentOf(node))) {
//              if(node == rightOf(parentOf(node))) {
//                  node.left = parentOf(node);
//                  node.father = grandParentOf(node);
//                  grandParentOf(node).left = node;
//                  parentOf(node).father = node;
//              }
//
//          }
//
//
//    }


//    /**
//     * Verifica os possiveis casos que necessitam fazer uma rotação para a direita.
//     * Recebe como parametro o nodo que foi recentemente adicionado.
//     */
//    public void rotateRight(Node node) {
//        Node aux = node;
//        Node parent = node.father;
//        Node grandparent = parent.father;
//
//
//        if(grandparent.right.cor == Cor.RED && grandparent.right == parent && grandparent.left.cor == Cor.BLACK) {
//            aux.right = parent;
//            aux.father = grandparent;
//            grandparent.right = aux;
//            parent.father = aux;
//        }
//
//        if(grandparent.cor == Cor.BLACK
//                && grandparent.left == parent && parent.cor == Cor.RED
//                && parent.left == aux && aux.cor == Cor.RED) {
//            //Falta verificar se o Nodo (grandparent) é a raiz da arvore.
//            parent.right = grandparent;
//            parent.father = grandparent.father;
//            grandparent.father = parent;
//            parent.changeColor();
//            grandparent.changeColor();
//        }
//
//    }


    /**
     * Verifica os dois possiveis casos que possam ocorrer aonde exista dois nodos vermelhos e não que não se encaixem nos casos para fazer rotação.
     * Método troca a cor dos nodos para vermelho.
     * Recebe como parametro o nodo que foi recentemente adicionado.
     * */
//    public void recoloringNodes(Node node) {
//        Node aux = node;
//        Node parent = node.father;
//        Node grandparent = parent.father;
//        Node uncleLeft = grandparent.left;
//        Node uncleRight = grandparent.right;
//
//        if(grandparent == root) {
//            grandparent.cor = Cor.BLACK;
//        }
//
//        if(grandparent.left.cor == Cor.RED && grandparent.right.cor == Cor.RED) {
//            grandparent.right.changeColor();
//            grandparent.left.changeColor();
//            if(grandparent != root)
//                grandparent.changeColor();
//        }
//
//        if(grandparent.cor == Cor.BLACK
//                && grandparent.left == uncleLeft && uncleLeft.cor == Cor.RED
//                && grandparent.right == parent && parent.cor == Cor.RED
//                && parent.right == aux) {
//
//            uncleLeft.changeColor();
//            parent.changeColor();
//            if(grandparent != root)
//                grandparent.changeColor();
//        }
//
//        if(grandparent.cor == Cor.BLACK
//                && grandparent.right == uncleRight && uncleRight.cor == Cor.RED
//                && grandparent.left == parent && parent.cor == Cor.RED
//                && parent.left == aux && aux.cor == Cor.RED) {
//
//            uncleRight.changeColor();
//            parent.changeColor();
//            if(grandparent != root)
//                grandparent.changeColor();
//        }
//    }

    /**
     * Método que faz o andamento pela arvore.
     * Recebe como parametros o elemento da arvore e o nodo alvo (sendo como padrão a raiz).
     * Retorna null se não for possivel achar a referencia para o nodo do elemento passado por parametro.
     */
    public Node searchNodeRef(Integer element, Node target) {
        int r;

        if (element == null || target == null) {
            return null;
        }

        r = target.element.compareTo(element);

        if (r == 0) {
            return target;
        } else if (r > 0) {
            return searchNodeRef(element, target.left);
        } else {
            return searchNodeRef(element, target.right);
        }
    }


    public int size() {
        return count;
    }

    /**
     * Método que verifica se o elemento passado por parametro está presente na arvore.
     * Retorna false caso o elemento não esteja na arvore.
     */
    public boolean contains(Integer element) {
        Node aux = searchNodeRef(element, root);
        if (aux == null) {
            return false;
        }
        return aux.element.equals(element);
    }

    /**
     * Método publico que faz a chamado do getHeight.
     */
    public int height() {
        if (isEmpty())
            return 0;
        int countRight = getHeight(root.right);
        int countLeft = getHeight(root.left);
        return countRight > countLeft ? countRight : countLeft;
    }

    /**
     * Conta os nodos e calcula o tamanho de ambos os lados da raiz e retorna o maior dos dois.
     * Recebe como parametro o nodo alvo.
     */
    private int getHeight(Node target) {
        if (target == null) {
            return 0;
        } else {
            return 1 + Math.max(getHeight(target.left), getHeight(target.right));
        }
    }

    /**
     * Conta os nodos da arvore incluindo a raiz.
     * Retorna o numero de nodos.
     */
    public int countNodes(Node n) {
        if (n == null) {
            return 0;
        } else {
            return 1 + countNodes(n.left) + countNodes(n.right);
        }
    }

    public Integer getLeft(Integer element) {
        Node aux = searchNodeRef(element, root);
        if (aux != null) {
            if (aux.left != null) {
                return aux.left.element;
            }
        }
        return null;
    }

    public Integer getRight(Integer element) {
        Node aux = searchNodeRef(element, root);
        if (aux != null) {
            if (aux.right != null) {
                return aux.right.element;
            }
        }
        return null;
    }


    /**
     * Método não utilizado no trabalho.
     * Clona a arvore.
     */
    public BinaryTree clone() {
        BinaryTree bt = new BinaryTree();
        clone(root, bt);
        return bt;
    }

    private BinaryTree clone(Node n, BinaryTree b) {
        if (n != null) {
            b.add(n.element);
            clone(n.left, b);
            clone(n.right, b);
        }
        return b;
    }

    /**
     * Retorna a raiz da arvore.
     * Caso não tenha nenhum elemento retorna null.
     */
    public Node getRoot() {
        if (!isEmpty()) {
            return root;
        }
        return null;
    }


    /**
     * Verifica se a arvore está balanceada(Ter em ambas as subarvores da raiz pelo menos 1 nivel de diferença).
     */
    public boolean isBalanced() {
        if (isEmpty())
            return true;

        int max = Math.max(getHeight(root.left), getHeight(root.right));
        int min = Math.min(getHeight(root.left), getHeight(root.right));

        if ((max - min) == 1)
            return true;

        return false;
    }

    /**
     * Método que retorna o elemento pai do que foi passado por parametro.
     * Retorna null caso não seja possivel achar o pai do elemento.
     */
    public Integer getParent(Integer element) {
        Node aux = searchNodeRef(element, root);
        if (aux.father.element != null) {
            return aux.father.element;
        }
        return null;
    }

}
