import java.util.HashMap;
import java.util.LinkedList;

public class RBTree {

    public enum Cor {
        RED,
        BLACK,
        BLANK
    }

    private class Node {
        public Integer element;
        public Node father;
        public Node right;
        public Node left;
        public Cor cor;

        public Node(Integer element) {
            this.element = element;
            this.father = null;
            this.left = null;
            this.right = null;
            this.cor = Cor.BLANK;
        }
    }

    private Node root;
    private Node nill;
    private int count;
    private HashMap<Integer, String> cardapio = new HashMap<>();


    public RBTree() {
        this.count = 0;
        this.root = null;
        this.nill = new Node(0);
        changeColor(nill, Cor.BLACK);
    }

    public boolean setRoot(Integer element) {
        if (!isEmpty())
            return false;
        root = new Node(element);
        root.cor = Cor.BLACK;
        root.right = nill;
        root.left = nill;
        return true;
    }

    public boolean isEmpty() {
        return root == null;
    }


    public void clear() {
        root = null;
        count = 0;
    }

    public void add(Integer element, String s) {
        add(root, element);
        cardapio.put(element, s);
        count++;
    }

    public RBTree clone() {
        return (this);
    }

    private Node add(Node n, Integer element) {
        Node aux;
        if (n == null) {
            setRoot(element);
            aux = new Node(element);
            adjustRBTree(aux);
            return aux;
        }
        if (n == nill)
            return null;
        if (element > n.element) {
            if (n.right == nill) {
                n.right = new Node(element);
                n.right.father = n;
                n.right.right = nill;
                n.right.left = nill;
                aux = searchNodeRef(element, root);
                adjustRBTree(aux);
                return aux;
            } else {
                add(n.right, element);
            }
        }
        if (element < n.element) {
            if (n.left == nill) {
                n.left = new Node(element);
                n.left.father = n;
                n.left.right = nill;
                n.left.left = nill;
                aux = searchNodeRef(element, root);
                adjustRBTree(aux);
                return aux;
            } else {
                add(n.left, element);
            }
        }

        return null;
    }

    public String getComida(Integer element) {
        if (cardapio.containsKey(element)) {
            return cardapio.get(element);
        }
        return null;
    }

    public Node searchNodeRef(Integer element, Node target) {
        int r;

        if (element == null || target == null) {
            return null;
        }


        r = target.element.compareTo(element);

        if (r == 0) {
            return target;
        } else if (r > 0) {
            return searchNodeRef(element, target.left);
        } else {
            return searchNodeRef(element, target.right);
        }
    }


    public int size() {
        return 1 + countNodes(root);
    }

    public boolean contains(Integer element) {
        Node aux = searchNodeRef(element, root);
        if (aux == null) {
            return false;
        }
        return aux.element.equals(element);
    }

    public int height() {
        if (isEmpty())
            return 0;
        int max = Math.max(getHeight(root.left), getHeight(root.right));

        if (max > getHeight(root.left)) {
            return max;
        }
        return getHeight(root.left);
    }


    private int getHeight(Node target) {
        if (target == nill || target == null || target == root) {
            return 0;
        }

        return 1 + Math.max(getHeight(target.right), getHeight(target.right));

    }

    public int countNodes(Node n) {
        if (n == null || n == nill) {
            return 0;
        } else {
            return 1 + countNodes(n.left) + countNodes(n.right);
        }
    }


    public Node getRoot() {
        if (!isEmpty()) {
            return root;
        }
        return null;
    }

    public Integer getRootElement() {
        if (!isEmpty()) {
            return getRoot().element;
        }
        return null;
    }

    public boolean isBalanced() {
        if (isEmpty())
            return true;

        int max = Math.max(getHeight(root.left), getHeight(root.right));
        int min = Math.min(getHeight(root.left), getHeight(root.right));

        if ((max - min) == 1)
            return true;

        return false;
    }

    public void rotateLeft(Node n) {
        Node parent = parentOf(n);
        Node grandParent = grandParentOf(n);
        n.father = grandParent;
        n.left = parent;
        parent.father = n;
        grandParent.left = n;
    }

    public void rotateLeft2(Node n) {
        Node parent = parentOf(n);
        n.left = parent;
        n.father = parent.father;
        parent.father = n;
        parent.right = nill;
    }

    public void rotateRight(Node n) {
        Node parent = parentOf(n);

        n.right = parent;
        n.father = parent.father;
        parent.father = n;
        parent.left = nill;
    }

    public void rotateRight2(Node n) {
        Node grandParent = parentOf(n);

        n.left = grandParent;
        n.father = grandParent.father;
        grandParent.father = n;
    }

    public void adjustRBTree(Node n) {

        changeColor(n, Cor.RED);

        if (n != null && n != root && isRed(parentOf(n))) {

            if (isRed(getUncle(n))) {
                changeColor(parentOf(n), Cor.BLACK);
                changeColor(getUncle(n), Cor.BLACK);
                changeColor(grandParentOf(n), Cor.RED);
                adjustRBTree(grandParentOf(n));
            }
            if (parentOf(n) == leftOf(grandParentOf(n))) {
                if (n == rightOf(parentOf(n))) {
                    rotateLeft(n);
                    n = leftOf(n);
                }
                changeColor(parentOf(n), Cor.BLACK);
                changeColor(grandParentOf(n), Cor.RED);
                rotateRight(parentOf(n));
            }
            if (parentOf(n) == rightOf(grandParentOf(n))) {
                if (n == leftOf(parentOf(n))) {
                    rotateRight(n);
                    n = rightOf(n);
                }
                changeColor(parentOf(n), Cor.BLACK);
                changeColor(grandParentOf(n), Cor.RED);
                rotateLeft2(parentOf(n));
            }

        }

        changeColor(root, Cor.BLACK);

    }

    private Node rightOf(Node n) {
        if (n == null)
            return null;
        return n.right;
    }

    private Node leftOf(Node n) {
        if (n == null)
            return null;
        return n.left;
    }

    private Node parentOf(Node n) {
        if (n == null)
            return null;
        return n.father;
    }

    private Node grandParentOf(Node n) {
        if (n == null)
            return null;
        return parentOf(parentOf(n));
    }

    private Node getUncle(Node n) {
        if (n == null || parentOf(n) == null)
            return null;
        if (leftOf(grandParentOf(n)) == parentOf(n) && rightOf(grandParentOf(n)) != nill) {
            return rightOf(grandParentOf(n));
        }
        return leftOf(grandParentOf(n));
    }

    private Cor colorOf(Node n) {
        if (n == null)
            return null;
        return n.cor;
    }

    private boolean isRed(Node n) {
        if (n == null)
            return false;
        return n.cor == Cor.RED;
    }

    private boolean isBlack(Node n) {
        if (n == null)
            return false;
        return n.cor == Cor.BLACK;
    }

    private void changeColor(Node n, Cor cor) {
        if (n != null)
            n.cor = cor;
    }

    public String toString(Integer element) {
        Node aux = searchNodeRef(element, root);
        String str = "";
        str += "Elemento: " + aux.element + aux.cor;
        return str;
    }

    public Cor colorOf(Integer element) {
        Node aux = searchNodeRef(element, root);
        if (aux == null)
            return null;
        return aux.cor;
    }

    public Integer getParent(Integer element) {
        Node aux = searchNodeRef(element, root);
        return aux.father.element;
    }

    public LinkedList positionsPre() {
        LinkedList res = new LinkedList();
        positionsPreAux(root, res);
        return res;
    }

    private void positionsPreAux(Node n, LinkedList res) {
        if (n != null) {
            res.add(n.element); //Visita o nodo
            positionsPreAux(n.left, res); //Visita a subarvore esquerda
            positionsPreAux(n.right, res); //Visita a subarvore direita
        }
    }

    public LinkedList positionsCentral() {
        LinkedList res = new LinkedList();
        positionsCentralAux(root, res);
        return res;
    }

    private void positionsCentralAux(Node n, LinkedList res) {
        if (n != null) {
            positionsCentralAux(n.left, res);
            res.add(n.element);
            positionsCentralAux(n.right, res);
        }
    }

    public LinkedList positionsPos() {
        LinkedList res = new LinkedList();
        positionsPreAux(root, res);
        return res;
    }

    private void positionsPosAux(Node n, LinkedList res) {
        if (n != null) {
            positionsPosAux(n.left, res); //Visita a subarvore esquerda
            positionsPosAux(n.right, res); //Visita a subarvore direita
            res.add(n.element); //Visita o nodo
        }
    }

    public LinkedList<Node> positionsWidth() {
        LinkedList<Node> lista = new LinkedList<>();
        Queuee<Node> fila = new Queuee<>();
        Node n = root;
        fila.enqueue(n);
        while (!fila.isEmpty()) {
            n = fila.dequeue();
            lista.add(n);
            if (n.left != null) {
                fila.enqueue(n.left);
            }
            if (n.right != null) {
                fila.enqueue(n.right);
            }
        }
        return lista;
    }


}
